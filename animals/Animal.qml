import Qt 4.7

import Qt.labs.gestures 2.0


Rectangle {
    id: animalRectangle;
    property string image;

    color: "transparent"
    width: 100; height: 100;
    property int imageSize: width - 10;

    // to identify pairs of animals
    property int type;

    state: "back";

    states: [
        State {
            name: "back"
        },
        State {
            name: "front"
        },
        State {
            name: "solved";
            extend: "front";
        }
    ]

    transitions: [
        Transition {
            from: "*"
            to: "solved";
            ParallelAnimation {
            NumberAnimation {
                target: flipable;
                property: "angle";
                to: 180;
                duration: 1000
            }
            NumberAnimation { target: animalRectangle; property: "opacity"; to: 0.9; duration: 1000;}
        }
        },

        Transition {
            from: "back";
            to: "*";
            NumberAnimation {
                target: flipable;
                property: "angle";
                from: 0;
                to: 180;
                duration: 1000
            }
        },

        Transition {
            from: "front";
            to: "back";
            SequentialAnimation {
                ScriptAction {
                    script: console.log("transition");
                }
                NumberAnimation {
                    target: flipable;
                    property: "angle";
                    to: 180;
                    duration: 1000
                }
                PauseAnimation { duration: 1000 }
                NumberAnimation {
                    target: flipable;
                    property: "angle";
                    to: 0;
                    duration: 1000
                }
            }
        }
    ]

    Flipable {
        id: flipable;
        property int angle: 0;

        anchors.fill: parent;

        Rectangle {
            radius: 10;
            color: 'white';
            anchors.fill: parent;
            z: -1
        }
        front: Image {
            id: backImage;
            width: imageSize; height: imageSize; anchors.centerIn: parent;
            fillMode: "PreserveAspectFit";
            source: "images/qt-logo.png";
        }
        back: Image {
            id: animalImage;
            width: imageSize; height: imageSize; anchors.centerIn: parent;
            fillMode: "PreserveAspectFit";
            source: animalRectangle.image;
        }

        transform: Rotation {
            origin.x: flipable.width/2; origin.y: flipable.height/2
            axis.x: 0; axis.y: 1; axis.z: 0     // rotate around y-axis
            angle: flipable.angle
        }
    }

    GestureArea {
        anchors.fill: parent

        Tap {
            onFinished: animalRectangle.parent.animalTapped(animalRectangle);
        }

        TapAndHold {
            onStarted: console.log("tap-and-hold started");
            onFinished: console.log("tap-and-hold finished");
        }

        Pan {
            onStarted: animalRectangle.z = 1;
            onUpdated: {
                animalRectangle.x += gesture.delta.x;
                animalRectangle.y += gesture.delta.y;
            }
            onFinished: {
                animalRectangle.z = 0;

                console.log("velo: " + gesture.horizontalVelocity);
            }
        }

        Pinch {
            onStarted: animalRectangle.z = 1;
            onUpdated: {
                animalRectangle.rotation += gesture.rotationAngle - gesture.lastRotationAngle
                animalRectangle.scale = animalRectangle.scale * gesture.scaleFactor;
                animalRectangle.x += gesture.centerPoint.x - gesture.lastCenterPoint.x
                animalRectangle.y += gesture.centerPoint.y - gesture.lastCenterPoint.y
            }
            onFinished: animalRectangle.z = 0;
        }
    }
}
