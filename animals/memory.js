var component;
var maxRow = 3;
var maxColumn = 4;
var maxIndex = maxColumn * maxRow;
var finished = [];
var positions = [];
var allTiles = [];

var animals = [ "ArtFavor_Cartoon_Sheep.png", "danko_Friendly_rabbit.png",
                "lemmling_Cartoon_cow.png", "PeterM_Sad_cat.png",
                "bugmenot_Happy_Pig.png", "Gerald_G_Crawfish.png",
                "Machovka_lady_bug.png", "carlitos_Green_Worm.png",
                "Gerald_G_Rubber_Duck.png", "molumen_Green_sitting_frog.png"]

function reset() {
    positions = [];
    finished = [];

    for (var i = 0; i < allTiles.length; i++) {
        allTiles[i].destroy();
    }

    createBoard();
}

function createBoard() {
    var currentTime = new Date();
    var seed = currentTime.getSeconds() * currentTime.getHours();
    Math.random(seed);

    for (var i = 0; i < maxIndex; i++) {
        positions[i] = i;
        var position = Math.floor(Math.random()*positions.length);
        var tmp = positions[i];
        positions[i] = positions[position];
        positions[position] = tmp;
    }

    for (var i = 0; i < maxIndex/2; i++) {
        createAnimal(animals[i], i, positions[i*2]%maxColumn, Math.floor(positions[i*2]/maxColumn));
        createAnimal(animals[i], i, positions[i*2+1]%maxColumn, Math.floor(positions[i*2+1]/maxColumn));
    }
}

function createAnimal(image, animalId, column, row) {
    if (component == null)
        component = Qt.createComponent("Animal.qml");

    if (component.status == Component.Ready) {
        var dynamicObject = component.createObject(gameBoard);
        if (dynamicObject == null) {
            console.log("error creating block");
            console.log(component.errorString());
        }
        dynamicObject.type = animalId;
        dynamicObject.image = "images/" + image;
        dynamicObject.x = 5 + column * (gameBoard.blockSize+5);
        dynamicObject.y = 5 + row * (gameBoard.blockSize+5);
        dynamicObject.width = gameBoard.blockSize;
        dynamicObject.height = gameBoard.blockSize;
        allTiles.push(dynamicObject);
    } else {
        console.log("error loading block component");
        console.log(component.errorString());
    }
}


var visibleAnimal;
var lastClicked;
function tileClicked(animal) {
    if (animal.state == "solved") {
        console.log("I'm done already.");
        return;
    }
    if (animal.state == "front") {
        console.log("clicked on front");
        return;
    }

    if (animal == visibleAnimal) {
        console.log("tile already visible");
        return;
    }

    if (visibleAnimal == null) {
        console.log("first animal uncovered");
        visibleAnimal = animal;
        animal.state = "front";
        return;
    }

    // we got a second animal - is it a pair?
    if (visibleAnimal.type == animal.type) {
        MemoryLogic.finishedTiles(animal, visibleAnimal);
    } else {
        console.log("not the same type");
        visibleAnimal.state = "back";
        animal.state = "front";
        animal.state = "back";
    }
    visibleAnimal = null;
}

function finishedTiles(a1, a2) {
    finished.push(a1, a2);
    a1.state = "solved";
    a2.state = "solved";
    if (finished.length == maxIndex) {
        for (var i = 0; i < maxIndex; i++) {
            //finished[i].destroy();
            victoryText.visible = true;
        }
    } else {
        console.log("got " + finished.length + " of " + maxIndex);
    }
}

