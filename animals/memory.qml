import Qt 4.7
import Qt.labs.gestures 2.0

import "memory.js" as MemoryLogic

Rectangle {
    Component.onCompleted: MemoryLogic.createBoard();

    width: 1200
    height: 500

    id: gameBoard

    property int blockSize: 150;
    property bool started: false;

    gradient: Gradient {
        GradientStop {
            position: 0.00;
            color: "#0FFF1B";
        }
        GradientStop {
            position: 1.00;
            color: "#0070ff";
        }
    }

    Rectangle {
        id: victoryText
        visible: false;

        anchors.centerIn: parent;
        width: 200;
        height: 50;
        color: "white"
        border.color: "blue"
        opacity: 0.8;
        z: 10;

        Text {
            anchors.centerIn: parent
            font.pointSize: 24;
            text: "You win!";
        }
    }

    Rectangle {
        id: close;
        z: 100;
        width: 40;
        height: 40;
        color: "white";
        opacity: 0.7;
        border.color: "black";
        radius: 5;
        anchors.right: parent.right;
        anchors.top: parent.top;

        Text {
            anchors.centerIn: parent
            text:  "x";
            font.pointSize: 24;
        }

        GestureArea {
            anchors.fill: parent;
            TapAndHold {
                onFinished: {
                    console.log("close");
                    Qt.quit();
                }
            }
        }
    }

    Rectangle {
        id: restart;
        z: 100;
        width: 120;
        height: 60;
        color: "white";
        opacity: 0.7;
        border.color: "black";
        radius: 5;
        anchors.right: parent.right;
        anchors.bottom: parent.bottom;

        Text {
            anchors.centerIn: parent
            text:  "restart";
            font.pointSize: 24;
        }

        GestureArea {
            anchors.fill: parent;
            TapAndHold {
                onFinished: {
                    console.log("restart");
                    victoryText.visible = false;
                    MemoryLogic.reset();
                }
            }
        }
    }

    function animalTapped(animal) {
        MemoryLogic.tileClicked(animal);
    }
}


