import Qt 4.7
import Qt.labs.gestures 2.0


Column {
    id: column
    width: parent.width
    Repeater {
        model: 10
        Button {
            width: column.width
            property real r: Math.random()
            property real g: Math.random()
            property real b: Math.random()
            color: Qt.rgba(r,g,b,1)
            text: "Button " + index
        }
    }
    Rectangle {
        height: 100
        border.width: 1
        width: column.width
        color: "grey"

        Text {
            anchors.fill:  parent
            id: buttonText
            text: "Exit"
            font.pointSize: 20
        }

        GestureArea {
            anchors.fill: parent
            Tap {
                onFinished: {
                    Qt.quit()
                }
            }
        }
    }
}
