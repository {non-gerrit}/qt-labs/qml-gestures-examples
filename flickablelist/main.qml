import Qt 4.7
import Qt.labs.gestures 2.0

Rectangle {
    width: 200
    height: 200
    color: "#ff0000"

    Flickable {
        id: flickable
        anchors.fill: parent
        contentWidth: flicktarget.width; contentHeight: flicktarget.height
        flickableDirection: "VerticalFlick"

        Cols {
            id: flicktarget
            width: flickable.width
        }

    }
}
