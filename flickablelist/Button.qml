import Qt 4.7
import Qt.labs.gestures 2.0


Rectangle {
    width: parent.width
    height: 100
    color: "grey"
    border.width: 1

    property string text
    property int tapCounter: 0
    property int tapAndHoldCounter: 0
    property int swipeCounter: 0

    function isHorizontalSwipe(angle) {
        if (angle > 150 && angle < 210)
            return true;
        if (angle > 330 || angle < 30)
            return true;
        return false;
    }


    Text {
        anchors.fill:  parent
        id: buttonText
        text: "-Tap:" + parent.tapCounter + "\n-TapAndHold:" + parent.tapAndHoldCounter + "\n-Swipe:" + parent.swipeCounter
        font.pointSize: 20
    }

    GestureArea {
        anchors.fill: parent
        Tap {
            when: true
            onStarted: {
                console.log("--tap started")
            }
            onUpdated: {
                console.log("--tap updated")
            }
            onFinished: {
                console.log("--tap finished")
                parent.parent.tapCounter++
            }
            onCanceled: {
                console.log("--tap canceled")
            }
        }

        TapAndHold {
            when: true
            onStarted: {
                console.log("--tapAndHold started")
            }
            onUpdated: {
                console.log("--tapAndHold updated")
            }
            onFinished: {
                console.log("--tapAndHold finished")
                parent.parent.tapAndHoldCounter++
            }
            onCanceled: {
                console.log("--tapAndHold canceled")
            }
        }

        Swipe {
            when: isHorizontalSwipe(gesture.swipeAngle)
            onStarted: {
                console.log("--swipe started")
            }
            onUpdated: {
                console.log("--swipe updated")
            }
            onFinished: {
                console.log("--swipe finished:" + gesture.swipeAngle)
                parent.parent.swipeCounter++
            }
            onCanceled: {
                console.log("--swipe canceled")
            }
        }
    }

}
