import Qt 4.7
import Qt.labs.gestures 2.0

Rectangle {

    id: mainrect

    width: 380
    height: 600

    function foo(index) {
        var colors = ["blue", "green", "grey", "red", "lightgreen"];
        return colors[index % colors.length]
    }

    Rectangle {
        id: list
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height

        Repeater {
            model: [ "apple", "orange", "pineapple", "coconut", "foo", "bar", "baz", "zealot" ]

            Button {
                x: 10
                y: 10 + (height + 2) * index
                bgColor: foo(index)
                text: modelData
            }
        }
    }
    GestureArea {
        anchors.fill: parent
        property bool panEnabled : false

        Pan {
            onUpdated: {
                if (parent.panEnabled)
                    list.y += gesture.delta.y
                else if (Math.abs(gesture.offset.y) >= 10 && Math.abs(gesture.offset.x) < 10)
                    parent.panEnabled = true
            }
            onFinished: {
                parent.panEnabled = false
            }
        }
    }
}
