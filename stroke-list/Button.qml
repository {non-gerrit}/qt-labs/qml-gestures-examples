import Qt 4.7
import Qt.labs.gestures 2.0

Rectangle {
    id: button

    property string text : "Press me, Hold me, Strike me"
    property int counter : 0
    property string bgColor: "steelblue"
    property bool marked : false

    property bool enabled : state != "disabled"

    width: 360
    height: 40
    radius: 10

    Rectangle {
        id: background
        anchors.fill: parent
        radius: 10
        opacity: 0.3
        color: parent.bgColor
    }

    Rectangle {
        id: mark
        x: 10
        y: 5
        width: 0
        height: 0
        radius: 30
        color: "black"
        opacity: 1.0
        smooth: true
    }

    function toggleMark() {
        button.marked = !button.marked
        if (button.marked) {
            mark.width = 30
            mark.height = 30
        } else {
            mark.width = 0
            mark.height = 0
        }
    }

    function increaseCounter() {
        counter = counter + 1
    }

    Text {
        id: labelText
        text: parent.text + (counter ? " "+counter : "")
        anchors.centerIn: parent
    }

    Rectangle {
        id: stroke

        x: parent.width * 0.3 / 2
        anchors.verticalCenter: parent.verticalCenter
        width: 0
        height : 10

        color: "red"
        opacity: 1.0
    }


    states: [
        State {
            name: ""
            PropertyChanges { target: background; opacity: 0.3 }
            PropertyChanges { target: labelText; opacity: 1.0 }
            PropertyChanges { target: stroke; width: 0 }
        },
        State {
            name: "pressed"
            PropertyChanges { target: background; opacity: 1.0 }
        },
        State {
            name: "disabled"
            PropertyChanges { target: background; opacity: 0.3 }
            PropertyChanges { target: mark; opacity: 0.3 }
            PropertyChanges { target: labelText; opacity: 0.3 }
            PropertyChanges { target: stroke; width: stroke.parent.width * 0.7 }
        }
    ]
    transitions: [
        Transition {
            from: "*"
            to: "disabled"
            PropertyAnimation { target: stroke; duration: 1000; property: "width" }
        },
        Transition {
            from: "disabled"
            to: ""
            PropertyAnimation { target: stroke; duration: 1000; property: "width" }
        }
    ]

    property bool handled: false

    function isHorizontalSwipe(angle) {
        if (angle > 150 && angle < 210)
            return true;
        if (angle > 330 || angle < 30)
            return true;
        return false;
    }

    GestureArea {
        anchors.fill: parent

        Tap {
            when: button.enabled
            onStarted: { button.state = "pressed" }
            onCanceled: {
                if (!button.handled) {
                    button.state = "";
                }
            }
            onFinished: {
                if (!button.handled) {
                    button.state = "";
                    button.increaseCounter();
                }
                button.handled = false;
            }
        }

        TapAndHold {
            when: button.enabled
            onFinished: { console.log("tap-and-hold"); button.toggleMark() }
        }

        Swipe {
            when: gesture.speed > 25
            onStarted: button.handled = true
            onFinished: {
                if (isHorizontalSwipe(gesture.swipeAngle)) {
                    if (gesture.horizontalDirection == SwipeGesture.Right) {
                        button.state = "disabled"
                    } else if (gesture.horizontalDirection == SwipeGesture.Left) {
                        script: button.state = ""
                    }
                }
                button.handled = false
            }
        }
    }
}

