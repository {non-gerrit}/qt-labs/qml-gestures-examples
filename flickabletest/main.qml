import Qt 4.7
import Qt.labs.gestures 2.0

Rectangle {
    id: topWin
    width: 200
    height: 200
    color: "#ff0000"

    function calculateBoundingBox(angle) {
        angle = angle*Math.PI/180
        var sin = Math.abs(Math.sin(angle))
        var cos = Math.abs(Math.cos(angle))
        flicktarget.height = (sin*pinchable.width) + (cos*pinchable.height)
        flicktarget.width = (sin*pinchable.height) + (cos*pinchable.width)
    }


    Flickable {
        id: flickable
        anchors.fill: parent
        contentWidth: flicktarget.width; contentHeight: flicktarget.height

        GestureArea {
            anchors.fill: parent

            Pinch {
                onUpdated: {
                    if (gesture.changeFlags & PinchGesture.RotationAngleChanged)
                        pinchable.rotation += gesture.rotationAngle - gesture.lastRotationAngle
                    if (gesture.changeFlags & PinchGesture.ScaleFactorChanged) {
                        pinchable.width *= gesture.scaleFactor
                        pinchable.height *= gesture.scaleFactor
                    }
                    calculateBoundingBox(pinchable.rotation)
                }
            }
        }

        Rectangle {
            id: flicktarget
            width:  topWin.width
            height: topWin.height

            Image {
                id: pinchable
                source: "image2.png"
                anchors.centerIn: parent
                width:  topWin.width
                height:  topWin.height

                clip: true
            }

        }


    }

    Rectangle {
        x: 826
        y: 0
        width: 40
        height: 40
        color: "grey"
        Text {
            anchors.centerIn: parent
            text: "exit"
            font.bold: true
            font.pointSize: 12
        }
        GestureArea {
            anchors.fill: parent
            Tap {
                onFinished: {
                    Qt.quit()
                }
            }
        }
    }
}


