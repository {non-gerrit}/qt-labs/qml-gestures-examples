import Qt 4.7
import Qt.labs.gestures 2.0

Rectangle {

    id: mainrect

    width: 380
    height: 600

    function foo(index) {
        var colors = ["blue", "green", "grey", "red", "lightgreen"];
        return colors[index % colors.length]
    }

    Rectangle {
        id: list

        Repeater {
            model: 20

            Rectangle {
                x: 0
                y: index * (height + 5)
                width: 100
                height: 100
                property int row : index
                Repeater {
                    model: 20
                    Rectangle {
                        x: index * (width + 5)
                        y: 0
                        width: 100
                        height: 100
                        color: foo(parent.row + index)
                    }
                }
            }
        }
    }
    PropertyAnimation { id: xAnim; target: list; property: "x"; duration: 1000; to: 100 }
    PropertyAnimation { id: yAnim; target: list; property: "y"; duration: 1000; to: 100 }

    GestureArea {
        anchors.fill: parent

        Tap {
            onStarted: {
                xAnim.stop();
                yAnim.stop();
            }
        }
        Pan {
            onStarted: {
                list.y += gesture.delta.y;
                list.x += gesture.delta.x;
            }
            onFinished: {
                if (gesture.horizontalVelocity != 0 && gesture.offset.x != 0) {
                    xAnim.to = list.x + gesture.offset.x;
                    xAnim.duration = 10000.0 * gesture.offset.x / gesture.horizontalVelocity;
                    xAnim.start();
                }
                if (gesture.verticalVelocity != 0 && gesture.offset.y != 0) {
                    yAnim.to = list.y + gesture.offset.y;
                    yAnim.duration = 10000.0 * gesture.offset.y / gesture.verticalVelocity;
                    yAnim.start();
                }
            }
        }
    }
}
