/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the QtDeclarative module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

import Qt 4.7
import Qt.labs.gestures 2.0

Item {
    id: myRow
    width: 340
    height: tapWidgetContainer.height

    property variant reparentWidget: myRow
    property variant topLevel: myRow
    BorderImage {
        id: tapWidgetContainer
        source: "images/columnbg.png"
        border.left: 20; border.top: 20
        border.right: 20; border.bottom: 20
        width: parent.width
        height: col.height + 100
        Column{
            id: col
            spacing: 20
            y:20
            x:20
            TapAndHoldWidget {
                id: firstWidget
                color: "green"
            }
            TapAndHoldWidget {
                id: secondWidget
                color: "blue"
            }
            TapAndHoldWidget {
                id: thirdWidget
                color: "green"
            }
            TapAndHoldWidget {
                id: fourthWidget
                color: "blue"
            }
            TapAndHoldWidget {
                id: fifthWidget
                color: "green"
            }
            TapAndHoldWidget {
                id: sixthWidget
                color: "blue"
            }
            TapAndHoldWidget {
                id: seventhWidget
                color: "green"
            }
            TapAndHoldWidget {
                id: eightWidget
                color: "blue"
            }
        }


        property int maxY : (tapWidgetContainer.childrenRect.height > topLevel.height) ?
                                0 :
                                  (topLevel.height - tapWidgetContainer.childrenRect.height)
        property int minY : (tapWidgetContainer.childrenRect.height > topLevel.height) ?
                                -(tapWidgetContainer.childrenRect.height - topLevel.height - 40) :
        0
    }

    GestureArea {
        id: rowGestureArea
        anchors.fill: tapWidgetContainer

        Pan {
            when: Math.abs(gesture.delta.y) > Math.abs(gesture.delta.x)
            onUpdated: {
                tapWidgetContainer.pos.y = tapWidgetContainer.pos.y + gesture.delta.y;

                if (tapWidgetContainer.pos.y > tapWidgetContainer.maxY) { // limit pan down
                    tapWidgetContainer.pos.y = tapWidgetContainer.maxY;
                }
                else if (tapWidgetContainer.pos.y < tapWidgetContainer.minY) {// limit pan up
                    tapWidgetContainer.pos.y = tapWidgetContainer.minY;
                }
            }
        }
    }

}
