import Qt 4.7
import Qt.labs.gestures 2.0

BorderImage {
    id: borderWidget
    anchors.fill: parent
    source: "images/columnbg.png"
    border.left: 20; border.top: 20
    border.right: 20; border.bottom: 20

    Item {
        id: mainrect
        anchors.fill: parent
        anchors.margins: 20
        clip: true

        function foo(index) {
            var colors = ["#4477bb", "#44bb77", "#eeeeee", "#bbeedd", "lightgreen"];
            return colors[index % colors.length]
        }

        Item {
            id: list
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height

            onHeightChanged: y = 0

            GestureArea {
                anchors.fill: parent
                Tap { //swallow a tap
                }
            }

            Repeater {
                model: [ "apple", "orange", "pineapple", "coconut", "foo", "bar", "baz", "zealot" ]

                Button {
                    x: 0
                    y: 0 + (height + 2) * index
                    bgColor: mainrect.foo(index)
                    text: modelData
                }
            }

            property int maxY : (mainrect.height < list.childrenRect.height) ?
                    0 :
                    (mainrect.height - list.childrenRect.height)
            property int minY : (mainrect.height < list.childrenRect.height) ?
                    -(list.childrenRect.height - mainrect.height) :
                    0
        }
        GestureArea {
            anchors.fill: parent
            property bool panEnabled : false

            Pan {
                onUpdated: {
                    if (parent.panEnabled)
                        list.y += gesture.delta.y
                    else if (Math.abs(gesture.offset.y) >= 10 && Math.abs(gesture.offset.x) < 10)
                        parent.panEnabled = true

                    if(list.y > list.maxY)
                        list.y = list.maxY
                    else if(list.y < list.minY)
                        list.y = list.minY
                }
                onFinished: {
                    parent.panEnabled = false
                }
            }
        }
    }
}
