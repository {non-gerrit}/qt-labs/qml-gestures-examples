/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the QtDeclarative module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

import Qt 4.7
import Qt.labs.gestures 2.0

Rectangle {
    width: 848
    height: 480
    id: dashboard

    Image{
        source: "images/bg.png"
        x:{rowWidgetContainer.x/2}
    }

    property int spacing: 44

    Item{
        id: rowWidgetContainer
        y:32
        width: 300
        height: parent.height - y

        Item { // Just for spacing
            id:leftMargin
            anchors.left: parent.left
            width:spacing
        }

        ColumnOne {
            id: firstRow
            anchors.left: leftMargin.right
            reparentWidget: dashboard
            topLevel: dashboard
        }

        ColumnTwo {
            id: secondRow
            anchors.left : firstRow.right
            reparentWidget: dashboard
            anchors.leftMargin: spacing
            topLevel: dashboard
        }

        ColumnThree {
            id: thirdRow
            anchors.left : secondRow.right
            anchors.leftMargin: spacing
            reparentWidget: dashboard
            topLevel: dashboard

        }

        ColumnFour {
            id: fourthRow
            anchors.left : thirdRow.right
            reparentWidget: dashboard
            anchors.leftMargin: spacing
            topLevel: dashboard
        }

        ColumnThree {
            id: fifthRow
            anchors.left : fourthRow.right
            anchors.leftMargin: spacing
            reparentWidget: dashboard
            topLevel: dashboard

        }

        ColumnFour {
            id: sixthRow
            anchors.left : fifthRow.right
            anchors.leftMargin: spacing
            reparentWidget: dashboard
            topLevel: dashboard
        }

        Item { // Just for spacing
            anchors.left: sixthRow.right
            width:spacing
        }

        property int maxX : (dashboard.width < rowWidgetContainer.childrenRect.width) ? 0 :
                (dashboard.width - rowWidgetContainer.childrenRect.width)
        property int minX : (dashboard.width < rowWidgetContainer.childrenRect.width) ?
                -(rowWidgetContainer.childrenRect.width - dashboard.width) : 0
    }

    GestureArea {
        anchors.fill: dashboard

        Pan {
            when: Math.abs(gesture.delta.y) < Math.abs(gesture.delta.x)
            onUpdated: {
                rowWidgetContainer.pos.x = rowWidgetContainer.pos.x + gesture.delta.x;

                if (rowWidgetContainer.pos.x > rowWidgetContainer.maxX) {
                    rowWidgetContainer.pos.x = rowWidgetContainer.maxX;
                }
                else if (rowWidgetContainer.pos.x < rowWidgetContainer.minX) {
                    rowWidgetContainer.pos.x = rowWidgetContainer.minX;
                }
            }
        }
    }
}
