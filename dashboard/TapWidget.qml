/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the QtDeclarative module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

import Qt 4.7
import Qt.labs.gestures 2.0

Item {
    id: testWidget
    width: 260
    height: 160
    property alias color: innerWidget.dummyColor
    property variant reparentWidget : testWidget

    Item {
        id:innerWidget
        anchors.fill: parent
        property color dummyColor;

        BorderImage {
            id:background
            source:"images/button.png"
            border.left: 10
            border.top: 10
            border.right: 10
            border.bottom: 10
            anchors.fill: parent
            opacity:0.8
        }

        Text {
            id: labelText
            text: "Tap me!!!"
            anchors.centerIn: parent
            font.pointSize: 14
        }

        MouseArea {
            id: mouse
            anchors.fill: parent
            hoverEnabled: true
        }

        transitions: Transition {
            PropertyAnimation {
                target: colorwidget
                properties: "opacity"
                duration:200
            }
        }
        states: [
            State {
                name: "fullscreen"
                PropertyChanges {
                    target: background
                    opacity:1.0
                }
                PropertyChanges {
                    target: colorwidget
                    opacity:0.4
                }
                PropertyChanges {
                    target: labelText
                    text:"Full Screen Widget"
                }
            }
        ]

        GestureArea {
            id: tapGestureArea
            anchors.fill: parent
            Tap {
                onFinished: {
                    if (!(innerWidget.state == "fullscreen")) {
                        innerWidget.parent = testWidget.reparentWidget
                        innerWidget.anchors.fill = testWidget.reparentWidget
                        innerWidget.state = "fullscreen"
                    } else {
                        innerWidget.parent = testWidget
                        innerWidget.anchors.fill = testWidget
                        innerWidget.state = ""
                    }
                }
            }
        }
    }
}
