/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the QtDeclarative module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

import Qt 4.7
import Qt.labs.gestures 2.0

Item {
    id: me

    property string source : "images/qt-logo.png"
    width: 200
    height:  200
    clip: true

    BorderImage {
        id: name
        source: "images/columnbg.png"
        border.left: 20; border.top: 20
        border.right: 20; border.bottom: 20
        anchors.fill: parent
    }

    Image {
        id: img
        width: parent.width-20
        height: parent.height-20
        source: parent.source
        fillMode: Image.PreserveAspectFit
        transformOrigin: Item.Center
        x:10
        y:10
    }

    states: State {
        name: "collapsed"
        when:!parent.fullScreen
        PropertyChanges {
            target: img
            x:10
            y:10
        }
    }

    GestureArea {
        anchors.fill: parent

        Pinch {
            when: me.parent.fullScreen
            onUpdated: {
                if (gesture.changeFlags & PinchGesture.RotationAngleChanged)
                    img.rotation += gesture.rotationAngle - gesture.lastRotationAngle
                if (gesture.changeFlags & PinchGesture.ScaleFactorChanged)
                    img.scale *= gesture.scaleFactor
                if (gesture.changeFlags & PinchGesture.CenterPointChanged) {
                    img.x += gesture.centerPoint.x - gesture.lastCenterPoint.x
                    img.y += gesture.centerPoint.y - gesture.lastCenterPoint.y
                }
            }
        }
        Pan {
            when: me.parent.fullScreen
            onUpdated: {
                img.x += gesture.delta.x
                img.y += gesture.delta.y
            }
        }
    }
}
