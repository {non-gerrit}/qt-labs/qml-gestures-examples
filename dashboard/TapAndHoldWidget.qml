/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the QtDeclarative module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

import Qt 4.7
import Qt.labs.gestures 2.0

Item{
    id: button

    property string text : "Press me, Hold me"
    property int counter : 0
    property string bgColor: "steelblue"

    property bool enabled : state != "disabled"
    property color color : "red"

    width: 300
    height: 80

    Rectangle {
        id:coloritem
        anchors.fill:parent
        color: parent.color
        opacity:0.1
        visible:false
    }

    BorderImage {
        id:background
        source:"images/button.png"
        anchors.margins: -6
        border.left: 10
        border.top: 10
        border.right: 10
        border.bottom: 10
        anchors.fill: parent
        opacity:1
    }

    Rectangle {
        id: mark
        anchors.verticalCenter: button.verticalCenter
        x: 10
        width: button.height/2
        height: button.height/2
        radius: button.height/2
        color: "black"
        opacity: 1.0
        smooth: true
        visible: false
    }

    function toggleMark() {
        mark.visible = !mark.visible
    }

    function increaseCounter() {
        counter = counter + 1
    }

    Text {
        id: labelText
        text: parent.text + (counter ? " "+counter : "")
        anchors.centerIn: parent
        font.pointSize: 14
    }

    states: [
        State {
            name: "disabled"
            PropertyChanges { target: background; opacity: 0.8 }
            PropertyChanges { target: mark; opacity: 0.3 }
            PropertyChanges { target: labelText; opacity: 0.3 }
            PropertyChanges { target: stroke; width: stroke.parent.width * 0.7 }
        }
    ]
    transitions: [
        Transition {
            PropertyAnimation { duration: 160; property: "opacity" }
        }
    ]

    property bool handled: false

    function isHorizontalSwipe(angle) {
        if (angle > 150 && angle < 210)
            return true;
        if (angle > 330 || angle < 30)
            return true;
        return false;
    }

    GestureArea {
        anchors.fill: parent

        Tap {
            when: button.enabled
            onStarted: { button.state = "pressed" }
            onCanceled: {
                if (!button.handled) {
                    button.state = "";
                }
            }
            onFinished: {
                if (!button.handled) {
                    button.state = "";
                    button.increaseCounter();
                }
                button.handled = false;
            }
        }

        TapAndHold {
            when: button.enabled
            onFinished: { console.log("tap-and-hold"); button.toggleMark() }
        }
    }
}


