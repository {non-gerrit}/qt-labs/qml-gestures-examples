/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the QtDeclarative module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

import Qt 4.7
import Qt.labs.gestures 2.0

Rectangle {
    id: testWidget
    width: 260
    height: 160
    property alias color: innerWidget.color
    property variant reparentWidget : testWidget

    Rectangle {
        id: innerWidget
        color: "steelblue"
        radius: 10
        anchors.fill: testWidget

        property bool fullScreen : false

        VolumeChannel {
            id: vol1
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            width: parent.width/5
            volumeValue: 30
        }
        VolumeChannel {
            id:vol2
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: vol1.right
            width: parent.width/5
            volumeValue: 40
        }
        VolumeChannel {
            id:vol3
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: vol2.right
            width: parent.width/5
            volumeValue: 60
        }
        VolumeChannel {
            id:vol4
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: vol3.right
            width: parent.width/5
            volumeValue: 20
        }
        VolumeChannel {
            id:vol5
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: vol4.right
            width: parent.width/5
            volumeValue: 50
        }




        GestureArea {
            id: tapGestureArea
            anchors.fill: parent
            Tap {
                onFinished: {
                    if (!innerWidget.fullScreen) {
                        innerWidget.fullScreen = true
                        innerWidget.parent = testWidget.reparentWidget
                        innerWidget.anchors.fill = testWidget.reparentWidget
                        vol1.updateFontSize(16)
                        vol2.updateFontSize(16)
                        vol3.updateFontSize(16)
                        vol4.updateFontSize(16)
                        vol5.updateFontSize(16)

                    }
                    else {
                        innerWidget.parent = testWidget
                        innerWidget.anchors.fill = testWidget
                        innerWidget.fullScreen = false
                        vol1.updateFontSize(8)
                        vol2.updateFontSize(8)
                        vol3.updateFontSize(8)
                        vol4.updateFontSize(8)
                        vol5.updateFontSize(8)
                    }
                }
            }
        }
    }
}
