import Qt 4.7
import Qt.labs.gestures 2.0

Item{
    id: button

    property string text : "Press me, Hold me, Strike me"
    property int counter : 0
    property string bgColor: "steelblue"

    property bool enabled : state != "disabled"
    width:labelText.width+20
    anchors.right: parent.right
    anchors.left: parent.left
    height: parent.height/3-1

    Rectangle {
        id: background
        anchors.fill: parent
        radius: 3
        color: parent.bgColor
        border.color: '#ffffffff'
        opacity:0.7
        border.width: 1
        anchors.margins: 2
    }

    Rectangle {
        id: mark
        anchors.verticalCenter: button.verticalCenter
        x: 10
        width: button.height/2
        height: button.height/2
        radius: button.height/2
        color: "black"
        opacity: 0.7
        smooth: true
        visible: false
    }

    function toggleMark() {
        if (mark.visible == false)
            mark.visible = true
        else
            mark.visible = false
    }

    function increaseCounter() {
        counter = counter + 1
    }

    Text {
        id: labelText
        text: parent.text + (counter ? " "+counter : "")
        anchors.centerIn: parent
        font.pointSize: 15
    }

    Rectangle {
        id: stroke
        x: parent.width * 0.3 / 2
        anchors.verticalCenter: parent.verticalCenter
        width: 0
        height : 10
        color: "green"
        opacity: 1.0
    }


    states: [
        State {
            name: ""
            PropertyChanges { target: background; opacity: 0.7 }
            PropertyChanges { target: stroke; width: 0 }
        },
        State {
            name: "pressed"
            PropertyChanges { target: background; opacity: 1.0 }
        },
        State {
            name: "disabled"
            PropertyChanges { target: background; opacity: 0.3 }
            PropertyChanges { target: mark; opacity: 0.3 }
            PropertyChanges { target: stroke; width: stroke.parent.width * 0.7 }
        }
    ]
    transitions: [
        Transition {
            from: "*"
            to: "disabled"
            PropertyAnimation { target: stroke; duration: 1000; property: "width" }
        },
        Transition {
            from: "disabled"
            to: ""
            PropertyAnimation { target: stroke; duration: 1000; property: "width" }
        }
    ]

    property bool handled: false

    function isHorizontalSwipe(angle) {
        if (angle > 150 && angle < 210)
            return true;
        if (angle > 330 || angle < 30)
            return true;
        return false;
    }

    GestureArea {
        anchors.fill: parent

        Tap {
            when: button.enabled
            onStarted: { button.state = "pressed" }
            onCanceled: {
                if (!button.handled) {
                    button.state = "";
                }
            }
            onFinished: {
                if (!button.handled) {
                    button.state = "";
                    button.increaseCounter();
                }
                button.handled = false;
            }
        }

        TapAndHold {
            when: button.enabled
            onFinished: { console.log("tap-and-hold"); button.toggleMark() }
        }

        Swipe {
//            when: gesture.speed > 25
            onStarted: button.handled = true
            onFinished: {
                if (isHorizontalSwipe(gesture.swipeAngle)) {
                    if (gesture.horizontalDirection == SwipeGesture.Right) {
                        button.state = "disabled"
                    } else if (gesture.horizontalDirection == SwipeGesture.Left) {
                        button.state = ""
                    }
                }
                button.handled = false
            }
        }
    }
}

